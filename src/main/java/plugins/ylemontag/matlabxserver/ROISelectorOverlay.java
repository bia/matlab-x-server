package plugins.ylemontag.matlabxserver;

import icy.canvas.Canvas2D;
import icy.canvas.IcyCanvas;
import icy.painter.Overlay;
import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.sequence.SequenceAdapter;
import icy.util.GraphicsUtil;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.LinkedList;
import java.util.List;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Painter proposing to create a new ROI on the sequence or to select one 
 */
public class ROISelectorOverlay extends Overlay
{
	/**
	 * Interface to implement to listen events sent by the painter
	 */
	public static interface Listener
	{
		/**
		 * Click on the validation button
		 */
		public void onOkClicked(ROI selectedRoi);
		
		/**
		 * Click on the cancel button
		 */
		public void onCancelClicked();
	}
	
	private static final int NORMAL   = 0;
	private static final int HOVER    = 1;
	private static final int DISABLED = 2;
	
	private Sequence _sequence;
	private List<Listener> _listeners;
	private String _labelInfo;
	private String _labelOk  ;
	private String _labelCl  ;
	private Color[] _bg;
	private Color[] _fg;
	private boolean _hintPositionComputed;
	private int _xInfo;
	private int _xOk  ;
	private int _xCl  ;
	private int _yInfo;
	private int _yOk  ;
	private int _yCl  ;
	private Dimension _sizeInfo;
	private Dimension _sizeOk  ;
	private Dimension _sizeCl  ;
	private boolean _hoverOk;
	private boolean _hoverCl;
	
	/**
	 * Constructor with default text label
	 */
	public ROISelectorOverlay(Sequence sequence)
	{
		this(sequence, "");
	}
	
	/**
	 * Constructor
	 */
	public ROISelectorOverlay(Sequence sequence, String instruction)
	{
		super("Matlab ROI selector", OverlayPriority.TEXT_NORMAL);
		if(sequence==null) {
			throw new IllegalArgumentException("Argument 'sequence' cannot be null.");
		}
		_listeners = new LinkedList<Listener>();
		_labelInfo = instruction.isEmpty() ? "Select an existing ROI or create a new one." : instruction;
		_labelOk   = "Validate";
		_labelCl   = "Cancel";
		_bg = new Color[] { new Color(0,128,255), new Color(255,64,0), Color.GRAY };
		_fg = new Color[] { Color.WHITE         , Color.WHITE        , Color.BLACK};
		_hoverOk = false;
		_hoverCl = false;
		_hintPositionComputed = false;
		_sequence = sequence;
		_sequence.addPainter(this);
		_sequence.addListener(new SequenceAdapter()
		{
			@Override
			public void sequenceClosed(Sequence sequence)
			{
				fireCancel();
			}
		});
	}
	
	/**
	 * Add a new listener
	 */
	public void addListener(Listener l)
	{
		_listeners.add(l);
	}
	
	/**
	 * Remove a listener
	 */
	public void removeListener(Listener l)
	{
		_listeners.remove(l);
	}

	@Override
	public void paint(Graphics2D g0, Sequence sequence, IcyCanvas canvas)
	{
		if(g0==null) return;
		Graphics2D g = null;
		try {
			g = (Graphics2D)g0.create();
			
			// Use the viewer coordinates
			g.transform(((Canvas2D)canvas).getInverseTransform());
			
			// Compute the position of the hints
			ensureHintPositionComputed(g);
			
			// Draw the hints
			int stateOk = _sequence.getSelectedROI()==null ? DISABLED : (_hoverOk ? HOVER : NORMAL);
			int stateCl = _hoverCl ? HOVER : NORMAL;
			GraphicsUtil.drawHint(g, _labelInfo, _xInfo, _yInfo, _bg[NORMAL ], _fg[NORMAL ]);
			GraphicsUtil.drawHint(g, _labelOk  , _xOk  , _yOk  , _bg[stateOk], _fg[stateOk]);
			GraphicsUtil.drawHint(g, _labelCl  , _xCl  , _yCl  , _bg[stateCl], _fg[stateCl]);
		}
		
		// Release the graphics object
		finally {
			if(g!=null) {
				g.dispose();
			}
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e, Point2D imagePoint, IcyCanvas canvas)
	{
		if(e.isConsumed()) {
			return;
		}
		if(_hoverOk || _hoverCl) { // Two different mouse events are generated when
			e.consume();             // clicking on the mouse: mousePressed and
		}                          // mouseClick. These two events must be consumed
	}                            // together to avoid strange behaviors.
	
	@Override
	public void mouseClick(MouseEvent e, Point2D imagePoint, IcyCanvas canvas)
	{
		if(e.isConsumed()) {
			return;
		}
		if(_hoverOk) {
			e.consume();
			fireOk();
		}
		else if(_hoverCl) {
			e.consume();
			fireCancel();
		}
	}

	@Override
	public void mouseMove(MouseEvent e, Point2D imagePoint, IcyCanvas canvas)
	{
		if(refreshHover(e)) {
			_sequence.painterChanged(this);
		}
	}
	
	/**
	 * Computes the position where the various hints should be displayed
	 */
	private void ensureHintPositionComputed(Graphics2D g)
	{
		if(_hintPositionComputed) {
			return;
		}
		
		// Position and size of the label
		_xInfo = 3;
		_yInfo = 3;
		_sizeInfo = GraphicsUtil.getHintSize(g, _labelInfo);
		
		// Position and size of the cancel button
		_xCl = 3;
		_yCl = 6 + _sizeInfo.height;
		_sizeCl = GraphicsUtil.getHintSize(g, _labelCl);
		
		// Position of the validation button
		_xOk = 6 + _sizeCl.width;
		_yOk = _yCl;
		_sizeOk = GraphicsUtil.getHintSize(g, _labelOk);
		
		// Done!
		_hintPositionComputed = true;
	}
	
	/**
	 * Refresh the hover flags (that indicate whether the mouse cursor is over
	 * the OK/cancel buttons or not)
	 * @return true if at least one the two flags has changed
 	 */
	private boolean refreshHover(MouseEvent e)
	{
		// The size of the hints must have been computed before
		if(!_hintPositionComputed) {
			return false;
		}
		
		// Old values
		boolean oldHoverOk = _hoverOk;
		boolean oldHoverCl = _hoverCl;
		
		// Position of the cursor, in viewer coordinates
		int x = e.getX();
		int y = e.getY();
		
		// Refresh the flags
		_hoverOk = (x>=_xOk) && (y>=_yOk) && (x<_xOk+_sizeOk.width) && (y<_yOk+_sizeOk.height);
		_hoverCl = (x>=_xCl) && (y>=_yCl) && (x<_xCl+_sizeCl.width) && (y<_yCl+_sizeCl.height);
		return oldHoverOk!=_hoverOk || oldHoverCl!=_hoverCl;
	}
	
	/**
	 * Fire the listener, cancel event
	 */
	private void fireCancel()
	{
		_sequence.removePainter(this);
		for(Listener l : _listeners) {
			l.onCancelClicked();
		}
	}
	
	/**
	 * Fire the listener, OK event
	 */
	private void fireOk()
	{
		ROI roi = _sequence.getSelectedROI();
		if(roi==null) {
			return;
		}
		_sequence.removePainter(this);
		for(Listener l : _listeners) {
			l.onOkClicked(roi);
		}
	}
}
