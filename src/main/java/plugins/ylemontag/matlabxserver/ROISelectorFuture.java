package plugins.ylemontag.matlabxserver;

import icy.roi.ROI;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * @author Yoann Le Montagner
 * 
 * Wrap a mutex and a condition, to make 
 */
public class ROISelectorFuture
{
	ReentrantLock _lock       ;
	Condition     _condition  ;
	boolean       _isReady    ;
	ROI           _selectedRoi;
	
	/**
	 * Constructor
	 */
	public ROISelectorFuture(ROISelectorOverlay overlay)
	{
		_lock      = new ReentrantLock();
		_condition = _lock.newCondition();
		_isReady   = false;
		overlay.addListener(new ROISelectorOverlay.Listener()
		{
			@Override
			public void onOkClicked(ROI selectedRoi)
			{
				onButtonClicked(selectedRoi);
			}
			
			@Override
			public void onCancelClicked()
			{
				onButtonClicked(null);
			}
		});
	}
	
	/**
	 * Make the current thread wait until the user clicks one of the button drawn
	 * by the ROI selector painter, and return the selected ROI if any
	 */
	public ROI getSelectedROI()
	{
		_lock.lock();
		try {
			while(!_isReady) {
				try {
					_condition.await();
				}
				catch (InterruptedException e) {}
			}
			return _selectedRoi;
		}
		finally {
			_lock.unlock();
		}
	}
	
	/**
	 * Action performed when the user clicks on one of the button drawn by
	 * the ROI selector painter
	 */
	private void onButtonClicked(ROI selectedRoi)
	{
		_lock.lock();
		try {
			_isReady     = true;
			_selectedRoi = selectedRoi;
			_condition.signalAll();
		}
		finally {
			_lock.unlock();
		}
	}
}
